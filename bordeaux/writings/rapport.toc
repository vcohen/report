\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {paragraph}{}{4}{section*.3}
\contentsline {section}{\numberline {2}State of the Art}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}\emph {Task-flow} Paradigm}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Runtime Systems and other Execution Supports}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}StarSs}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Harmony}{5}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}KAAPI}{5}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}StarPU}{6}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}DAGuE}{7}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Summary}{8}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Linear Algebra}{8}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}LAPACK and ScaLAPACK}{8}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}PLASMA and Dplasma}{8}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}MAGMA and MORSE}{8}{subsubsection.2.3.3}
\contentsline {subsection}{\numberline {2.4}The Cholesky factorization}{9}{subsection.2.4}
\contentsline {section}{\numberline {3}Conceptual Contribution}{11}{section.3}
\contentsline {section}{\numberline {4}Technical Contribution}{11}{section.4}
\contentsline {paragraph}{}{11}{section*.4}
\contentsline {subsection}{\numberline {4.1}Code generation}{12}{subsection.4.1}
\contentsline {paragraph}{}{12}{figure.7}
\contentsline {subsubsection}{\numberline {4.1.1}Parsing}{12}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Data management}{14}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Scheduling}{14}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Summary}{15}{subsection.4.2}
\contentsline {section}{\numberline {5}Evaluation}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Methodology}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Homogeneous platform}{15}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Heterogeneous platform}{16}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}One GPU}{16}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Two GPUs}{16}{subsubsection.5.3.2}
\contentsline {subsection}{\numberline {5.4}Summary}{17}{subsection.5.4}
\contentsline {section}{\numberline {6}Conclusion and Future Challenges}{17}{section.6}
\contentsline {subsection}{\numberline {6.1}Conclusion}{17}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Future Challenges}{18}{subsection.6.2}
\contentsline {paragraph}{Short-term}{18}{section*.6}
\contentsline {paragraph}{Mid-term}{18}{section*.7}
\contentsline {paragraph}{Long-term}{18}{section*.8}
\contentsline {section}{\numberline {7}Annexe}{19}{section.7}
